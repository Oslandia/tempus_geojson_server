# coding: utf-8

"""Module to load tempus plugins
"""

import os

import pytempus as tempus
tempus.init()


class Progress(tempus.ProgressionCallback):
    def __call__(self, percent, finished ):
        print ('{}...'.format(percent))


def load_plugin(name, progress, options):
    """Try to load the C++ plugin

    A plugin is compiled and has a such as libplugin_name.so.

    note: the *.so plugin must be in your LD_LIBRARY_PATH as libtempus.so
    """
    try:
        print('Trying to load C++ plugin...', name)
        plugin = tempus.PluginFactory.instance().create_plugin(
            name,
            progress,
            options)
        print('Created C++ plugin [{}]'.format(plugin.name))
        return plugin
    except RuntimeError:
        print('Failed... Now trying python...')
        raise

class EmptyPlugin(object):
    """Empty shell
    """
    @staticmethod
    def request(options):
        raise Exception("tempus plugin is not loaded!!")

def get_pg_connection_string(config):
    """
    Get pg connection string from config.
    """
    dbOptionsStr = 'dbname={dbname} user={user} password={password}'
    if config['host']:
        dbOptionsStr += ' host={host}'
    if config['port']:
        dbOptionsStr += ' port={port}'
    return dbOptionsStr.format(**config)

class PluginManager(object):
    """Tempus pluging manager
    """
    _instance = None

    def __init__(self, name, config):
        """
        config: dict
            Flask app config. Used for the 'database' connection
        """
        self._is_loaded = False
        self.name = name
        self.config = {"dbname": config['DBNAME'],
                       "user": config['DBUSER'],
                       "password": config.get('DBPASSWORD', None),
                       "host": config.get('DBHOST', 'localhost'),
                       "port": config.get('DBPORT', 5432)}
        if 'GRAPH_FILE' in config:
            if not os.path.isfile(config['GRAPH_FILE']):
                msg = "Graph dump file '{}' not found. Check your configuration file"
                raise IOError(msg.format(config['GRAPH_FILE']))
            self.config['graph_file'] = config['GRAPH_FILE']
        self.progress = Progress()

    def _make_instance(self):
        options = {'db/options': get_pg_connection_string(self.config)}
        if 'graph_file' in self.config:
            options['from_file'] = self.config['graph_file']
        return load_plugin(self.name, self.progress, options)

    def instance(self):
        if not self._is_loaded:
            self._instance = self._make_instance()
            self._is_loaded = True
        return self._instance

    def request(self, req):
        options = {'db/options': get_pg_connection_string(self.config)}
        p = self.instance().request(options)
        return p.process(req)

plugin = EmptyPlugin()
